# GraphQL Client

An ergonomic library for making graphql requests that **autocompletes field selections** (no need to explicitly write each field we want to return).

Features:
- Autocomplete field selection (based on schema)
- Validate arguments pre-flight
- Typescript support
- Ergonomic developer experience

## Example

```typescript
import GraphQLClient from '@tsuberim/graphql-client';

const client = new GraphQLClient(url);

// fetch a person by id (all fields)
const person = await client.query('PersonById', {id: 1234});

// fetch a person by id (only name)
const person = await client.query('PersonById', {id: 1234}, ['name']);

// perform a mutation
const person = await client.mutate('createPerson', {id: 1234, name: 'Daniel'})

// do a batch request
const {byId, byName} = await client.batch({
    byId: {method: 'PersonById', args: {id: 1234}},
    byName: {method: 'PersonByName', args: {name: 1234}},
})
```

## Contributing

Please submit issues and PRs for features that are missing!