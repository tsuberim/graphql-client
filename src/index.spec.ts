import { GraphQLSchema } from "graphql";
import { ApolloServer, gql } from 'apollo-server';
import { GraphQLClient } from ".";

describe('GraphQLClient', () => {
    const mocks = {
        Boolean: () => true,
        Int: () => 6,
        Float: () => 22.1,
        String: () => 'str',
    };

    let server: ApolloServer;
    let client: GraphQLClient;
    beforeAll(async () => {
        server = new ApolloServer({
            typeDefs: gql`
                type Address {
                    street: String!
                    number: Int!
                }

                type Person {
                    name: String!
                    address: Address
                }

                type Query {
                    hello: Person
                    world(a: Boolean, b: Float!): Int!
                }`,
            mocks
        });

        const { url } = await server.listen();
        client = new GraphQLClient(url);
    })

    afterAll(async () => {
        await server.stop()
    })

    it('should fetch schema', async () => {
        const schema = await client.getSchema();
        expect(schema instanceof GraphQLSchema);
    })

    it('should autocomplete fields', async () => {
        const result = await client.query('hello')
        expect(result).toEqual({
            name: mocks.String(),
            address: {
                street: mocks.String(),
                number: mocks.Int()
            }
        })
    })

    it('should include only selected fields', async () => {
        const result = await client.query('hello', {}, ['name'])
        expect(result).toEqual({
            name: mocks.String()
        })
    })

    it('should inline given variables', async () => {
        const result = await client.query('world', { a: true, b: 32.4 })
        expect(result).toEqual(mocks.Int());
    })

    it('should throw for unknown variable', async () => {
        await expect(client.query('world', { a: true, unknown: 'hello' })).rejects.toThrow('unknown')
    })

    it('should throw for incorrectly typed variable', async () => {
        await expect(client.query('world', { a: true, b: false })).rejects.toThrow('cannot represent')
    })

    it('should note throw for optional variable', async () => {
        await expect(client.query('world', { b: 32.4 })).resolves.toEqual(mocks.Int())
    })

    it('should batch multiple queries', async () => {
        const response = await client.batch({
            foo: {
                method: 'world',
                args: { a: false, b: 32.4 }
            },
            bar: {
                method: 'world',
                args: { a: true, b: 32.4 }
            }
        });
        expect(response.foo).toEqual(mocks.Int());
        expect(response.bar).toEqual(mocks.Int())
    })

    it('should throw for empty batch', async () => {
        await expect(client.batch({})).rejects.toThrow('at least one')
    })
})