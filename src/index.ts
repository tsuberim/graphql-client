#!/usr/bin/env node

import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { GraphQLSchema, getIntrospectionQuery, buildClientSchema, print, DocumentNode, Kind, coerceInputValue, OperationTypeNode, astFromValue, GraphQLOutputType, SelectionSetNode, GraphQLList, GraphQLNonNull, GraphQLObjectType, FieldNode, DefinitionNode } from 'graphql';

function mapValues<A, B>(obj: { [key: string]: A }, fn: (a: A, key: string) => B): { [key: string]: B } {
    const out: { [key: string]: B } = {};
    for (const [key, value] of Object.entries(obj)) {
        const val = fn(value, key);
        out[key] = val;
    }
    return out;
}

/**
 * Create a SelectionSetNode (or undefined) for a given GraphQLOutputType that selects all fields recursively
 * @param type a GraphQLOutputType
 * @param selection (optional) array of fields to only select those *top-level* fields instead of all fields
 * @returns
 */
export function createSelectionSet(type: GraphQLOutputType, selection?: string[]): SelectionSetNode | undefined {
    if (type instanceof GraphQLList || type instanceof GraphQLNonNull) {
        return createSelectionSet(type.ofType);
    } else if (type instanceof GraphQLObjectType) {
        const fields = type.getFields()
        return {
            kind: Kind.SELECTION_SET,
            selections: Object.entries(fields)
                .filter(([_, field]) => field.args.length === 0) // we can only select fields with no args
                .filter(([name]) => !selection || selection.includes(name)) // if there's a selection, filter only those fields
                .map(([name, field]) => ({
                    kind: Kind.FIELD,
                    name: { kind: Kind.NAME, value: name },
                    selectionSet: createSelectionSet(field.type)
                }))
        }
    } else {
        return undefined
    }
}

export function createFieldNode(schema: GraphQLSchema, operation: 'query' | 'mutation', alias: string, methodName: string, args: { [key: string]: any } = {}, selection?: string[]): FieldNode {
    const rootObjectType = operation === 'query' ? schema.getQueryType() : schema.getMutationType();

    // fetch relevant field
    const field = rootObjectType?.getFields()[methodName];
    if (!field) {
        throw new Error(`Unknown ${operation} '${methodName}'`)
    }

    const { args: fieldArgs, type } = field;
    const argTypes = Object.fromEntries(fieldArgs.map(arg => [arg.name, arg.type]));

    // validate unknown variables, also stip 'undefined's
    for (const name in args) {
        if (args[name] === undefined) {
            delete args[name]
        } else if (!argTypes[name]) {
            throw new Error(`Unknown argument '${name}' of ${operation} '${methodName}'`)
        }
    }


    // convert each variable to a ValueNode, coercing it to its appropriate type
    const values = mapValues(argTypes, (type, name) => {
        const val = coerceInputValue(args[name], type);
        const ast = astFromValue(val, type);
        if (!ast) {
            throw new Error(`Could not generate AST from JSON value ${JSON.stringify(val, undefined, 2)}`)
        }
        return ast
    })

    return {
        kind: Kind.FIELD,
        name: { kind: Kind.NAME, value: methodName },
        alias: { kind: Kind.NAME, value: alias },
        selectionSet: createSelectionSet(type, selection),
        arguments: Object.entries(values).map(([name, value]) => ({
            kind: Kind.ARGUMENT,
            name: { kind: Kind.NAME, value: name },
            value
        }))
    }
}

/**
 * Create a graphql documents from some query and mutation field selections
 * @param queries 
 * @param mutations 
 * @returns 
 */
export function createDocumentNode(queries: FieldNode[], mutations: FieldNode[] = []): DocumentNode {
    const defs: DefinitionNode[] = [];

    if (!queries.length && !mutations.length) {
        throw new Error(`Must provide at least one query or mutation`)
    }

    if (queries.length) {
        defs.push({
            kind: Kind.OPERATION_DEFINITION,
            operation: OperationTypeNode.QUERY,
            selectionSet: {
                kind: Kind.SELECTION_SET,
                selections: queries
            }
        })
    }

    if (mutations.length) {
        defs.push({
            kind: Kind.OPERATION_DEFINITION,
            operation: OperationTypeNode.MUTATION,
            selectionSet: {
                kind: Kind.SELECTION_SET,
                selections: mutations
            }
        })
    }

    return {
        kind: Kind.DOCUMENT,
        definitions: defs
    }
}

export interface FetchOptions {
    operation?: 'query' | 'mutation'
    method: string,
    args?: { [key: string]: any },
    selection?: string[]
}

export interface BatchOptions {
    [key: string]: FetchOptions
}

export class GraphQLClient {
    schema: GraphQLSchema | null = null;
    client: AxiosInstance
    constructor(url: string, axiosOptions?: AxiosRequestConfig) {
        this.client = axios.create({ ...axiosOptions, baseURL: url });
    }

    async getSchema(): Promise<GraphQLSchema> {
        if (!this.schema) {
            const { data: { data } } = await this.client.post('/', { query: getIntrospectionQuery() });
            const schema = buildClientSchema(data);
            this.schema = schema;
        }

        return this.schema!;
    }

    async batch<Batch extends BatchOptions>(batch: Batch): Promise<{ [key in keyof Batch]: any }> {
        const schema = await this.getSchema();

        const queries = Object.entries(batch)
            .filter(([_, { operation }]) => !operation || operation === 'query')
            .map(([name, { operation, method: methodName, args: variables, selection }]) => createFieldNode(schema, operation || 'query', name, methodName, variables, selection));

        const mutations = Object.entries(batch)
            .filter(([_, { operation }]) => operation === 'mutation')
            .map(([name, { operation, method: methodName, args: variables, selection }]) => createFieldNode(schema, operation || 'mutation', name, methodName, variables, selection));

        const doc = createDocumentNode(queries, mutations);

        // print the doc into a string and perform the query
        const query = print(doc)
        const { data: { data, errors } } = await this.client.post('/', { query });

        // handle errors
        if (errors) {
            throw new Error(`Failed with: ${errors.map((e: any) => e.message)}`)
        }

        return data;
    }

    async fetch(operation: 'query' | 'mutation', method: string, args: { [key: string]: any } = {}, selection?: string[]) {
        const { response } = await this.batch({ response: { operation, method, args, selection } })
        return response;
    }

    async query(methodName: string, args: { [key: string]: any } = {}, selection?: string[]) { return this.fetch('query', methodName, args, selection) }
    async mutate(methodName: string, args: { [key: string]: any } = {}, selection?: string[]) { return this.fetch('mutation', methodName, args, selection) }
}